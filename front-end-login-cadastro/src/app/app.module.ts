import { CadastroClienteService } from './cadastro/services/cadastro-cliente.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModaldadoscadastraisComponent } from './edit/modaldadoscadastrais/modaldadoscadastrais.component';
import { HttpClientModule } from '@angular/common/http';
import { DadosService } from '../app/edit/dados.service';
import { SharedModule } from './shared/shared.module';
import { LoginModule } from './login/login.module';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { CommonModule } from '@angular/common';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';

export const options: Partial<IConfig> | (() => Partial<IConfig>) | null = null;

@NgModule({
  declarations: [AppComponent, ModaldadoscadastraisComponent],

  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    SharedModule,
    LoginModule,
    NgxMaskModule.forRoot(),
    CommonModule,
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
  ],

  providers: [DadosService, CadastroClienteService, BsModalService],

  bootstrap: [AppComponent],
})
export class AppModule {}
