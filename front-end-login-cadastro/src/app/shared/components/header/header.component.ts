import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  currentUrl?: string;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  fazerLogin(): void {
    this.currentUrl = this.router.url;
    if (this.currentUrl === 'login') {
      this.router.navigate(['/login']);
    }

    alert('redicionando...');
  }
}
