import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './authentication/services/auth-guard.service';
import { RedefinirSenhaComponent } from './login/redefinir-senha/redefinir-senha.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'redefinir-senha', component: RedefinirSenhaComponent },
  {
    path: 'inicio',
    loadChildren: () =>
      import('./area-cliente/area-cliente.module').then(
        (m) => m.AreaClienteModule
      ),
    canActivate: [AuthGuardService],
  },
  {
    path: 'encerramento',
    loadChildren: () =>
      import('./encerramento/encerramento.module').then(
        (m) => m.EncerramentoModule
      ),
    canActivate: [AuthGuardService],
  },
  {
    path: 'cadastro',
    loadChildren: () =>
      import('./cadastro/cadastro.module').then((m) => m.CadastroModule),
  },
  {
    path: 'edit',
    loadChildren: () => import('./edit/edit.module').then((m) => m.EditModule),
    canActivate: [AuthGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
