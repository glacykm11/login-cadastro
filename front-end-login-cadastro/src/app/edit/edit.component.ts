import { Component, OnInit, Input, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ModaldadoscadastraisComponent } from '../edit/modaldadoscadastrais/modaldadoscadastrais.component';
import { CadastroClienteService } from '../cadastro/services/cadastro-cliente.service';
import { take } from 'rxjs/operators';
import { Client } from '../shared/interfaces/client';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {
  client: Client[] = [];

  constructor(
    private modalService: NgbModal,
    private cadastro: CadastroClienteService
  ) {}

  ngOnInit() {
    this.cadastro
      .get()
      .pipe(take(1))
      .subscribe((dados) => (this.client = dados));
  }

  openEdit() {
    const modalRef = this.modalService.open(ModaldadoscadastraisComponent);
    modalRef.componentInstance.name = 'World';
  }
}
