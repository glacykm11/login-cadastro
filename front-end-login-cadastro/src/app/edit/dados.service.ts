import { HttpClient,HttpErrorResponse } from "@angular/common/http";
import {Injectable} from "@angular/core"
import {Pessoa} from './Pessoa'
import { Observable, throwError } from "rxjs";
import { catchError, tap } from 'rxjs/operators';





@Injectable({
  providedIn: 'root'
})


export class DadosService {

  private productUrl ='api/pessoas/pessoas.json'

  constructor(private http: HttpClient){}

  getDados(): Observable<Pessoa[]>{
  
    return this.http.get<Pessoa[]>(this.productUrl).pipe(
      tap(data =>console.log('All', JSON.stringify(data))),
      catchError(this.handleError)
    );
  } 

private handleError(err:HttpErrorResponse){
  let errorMessage='';
  if(err.error instanceof ErrorEvent){
    errorMessage = `An error ocurred: ${err.error.message}`;
  } else {
    errorMessage = `Server returned code: ${err.status}, error message is${err.message}`;
  }

  console.error(errorMessage);
  return throwError(errorMessage);


}
}
     
    
 