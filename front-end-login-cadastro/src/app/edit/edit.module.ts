import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditComponent } from './edit.component';
import { DadosPessoaisComponent } from './dados-pessoais/dados-pessoais.component';
import { ContatoComponent } from './contato/contato.component';
import { EnderecoComponent } from './endereco/endereco.component';
import { OcupacaoComponent } from './ocupacao/ocupacao.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { IConfig, NgxMaskModule } from 'ngx-mask';
export const options: Partial<IConfig> | (() => Partial<IConfig>) | null = null;


const ROUTES: Routes = [
  { path: '', component: EditComponent },
  { path: 'contato', component: ContatoComponent },
  { path: 'dados-pessoais', component: DadosPessoaisComponent },
  { path: 'endereco', component: EnderecoComponent },
  { path: 'ocupacao', component: OcupacaoComponent },
];

@NgModule({
  declarations: [
    EditComponent,
    DadosPessoaisComponent,
    ContatoComponent,
    EnderecoComponent,
    OcupacaoComponent,
    
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(ROUTES),
    FormsModule,
    SharedModule,
    NgxMaskModule,
    CommonModule,
    NgxMaskModule.forRoot()
    
  ],
})
export class EditModule {}
