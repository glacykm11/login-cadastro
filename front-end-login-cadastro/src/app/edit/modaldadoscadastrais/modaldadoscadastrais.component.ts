import { Component, OnInit, Type, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Location } from '@angular/common';

import { CadastroClienteService } from '../../cadastro/services/cadastro-cliente.service';
import { Client } from '../../shared/interfaces/client';

@Component({
  selector: 'app-modaldadoscadastrais',
  templateUrl: './modaldadoscadastrais.component.html',
  styleUrls: ['./modaldadoscadastrais.component.css'],
})
export class ModaldadoscadastraisComponent implements OnInit {
  noedit: boolean = true;

  client: Client[] = [];

  constructor(
    public activeModal: NgbActiveModal,
    private cadastro: CadastroClienteService,
    private location: Location
  ) {}

  ngOnInit() {
    this.cadastro.get().subscribe((dados) => (this.client = dados));
  }

  saveData() {
    this.cadastro.edit(Number(this.client[0].id), this.client[0]).subscribe(
      (sucess) => {
        console.log('atualizado');
        console.log(this.client);
        const modalRef = this.activeModal.close(ModaldadoscadastraisComponent);
        location.reload();
      },

      (error) => {
        console.log('falha');
        console.log(this.client);
      }
    );
  }
}
