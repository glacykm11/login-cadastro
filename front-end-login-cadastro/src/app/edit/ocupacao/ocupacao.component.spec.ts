import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OcupacaoComponent } from './ocupacao.component';

describe('OcupacaoComponent', () => {
  let component: OcupacaoComponent;
  let fixture: ComponentFixture<OcupacaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OcupacaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OcupacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
