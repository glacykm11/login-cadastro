import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AreaClienteComponent } from './area-cliente.component';
import { SharedModule } from '../shared/shared.module';

const ROUTES: Routes = [{ path: '', component: AreaClienteComponent }];

@NgModule({
  declarations: [AreaClienteComponent],
  imports: [RouterModule, RouterModule.forChild(ROUTES), SharedModule],
})
export class AreaClienteModule {}
