import { Component, ContentChild, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormControlName, FormGroup, Validators} from '@angular/forms';
@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {

  validations_form: any;
  emailForm!: FormGroup;

  @ContentChild(FormControlName) control!: FormControlName;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {

    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
      ]))
    })

  }

  avancar (){
    if (!this.emailForm.valid) {
      return;
    }
    window.location.href = "../email";
  }
}


