import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuintaTelaComponent } from './quinta-tela.component';

describe('QuintaTelaComponent', () => {
  let component: QuintaTelaComponent;
  let fixture: ComponentFixture<QuintaTelaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuintaTelaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuintaTelaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
