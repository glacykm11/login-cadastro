/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CadastroClienteService } from './cadastro-cliente.service';

describe('Service: CadastroCliente', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CadastroClienteService],
    });
  });

  it('should ...', inject(
    [CadastroClienteService],
    (service: CadastroClienteService) => {
      expect(service).toBeTruthy();
    }
  ));
});
