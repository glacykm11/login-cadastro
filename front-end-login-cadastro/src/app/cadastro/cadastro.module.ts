import { FeedbackModalComponent } from './feedback-modal/feedback-modal.component';
import { ErrorMenssageComponent } from './error-menssage/error-menssage.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SegundaTelaComponent } from './segunda-tela/segunda-tela.component';
import { TerceiraTelaComponent } from './terceira-tela/terceira-tela.component';
import { QuartaTelaComponent } from './quarta-tela/quarta-tela.component';
import { QuintaTelaComponent } from './quinta-tela/quinta-tela.component';
import { CadastroComponent } from './cadastro.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from "@angular/common";
import { SharedModule } from '../shared/shared.module';
import { InputComponent } from './input/input.component';
import { NgxMaskModule } from 'ngx-mask';

const ROUTES: Routes = [
  { path: '', component: CadastroComponent },
  { path: 'segunda-pagina-cadastro', component: SegundaTelaComponent },
  { path: 'terceira-pagina-cadastro', component: TerceiraTelaComponent },
  { path: 'quarta-pagina-cadastro', component: QuartaTelaComponent },
  { path: 'quinta-pagina-cadastro', component: QuintaTelaComponent },
];

@NgModule({
  declarations: [
    CadastroComponent,
    SegundaTelaComponent,
    TerceiraTelaComponent,
    QuartaTelaComponent,
    QuintaTelaComponent,
    ErrorMenssageComponent,
    InputComponent,
    FeedbackModalComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(ROUTES),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    CommonModule,
    NgxMaskModule.forRoot(),
    
  ],
})
export class CadastroModule {}
