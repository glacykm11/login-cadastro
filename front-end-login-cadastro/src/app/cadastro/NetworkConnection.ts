import { Observable, fromEvent } from "rxjs";
import { map } from "rxjs/operators";


export enum ConnectionStatusEnum {
  Online,
  Offline
}

export class NetworkConnection {
 
  public static status: ConnectionStatusEnum = ConnectionStatusEnum.Online;
  private static online$: Observable<Event>;
  private static offline$: Observable<Event>;

  public static init() {
    NetworkConnection.online$ = fromEvent(window, 'online');
    NetworkConnection.offline$ = fromEvent(window, 'offline');

    console.log(NetworkConnection.online$)
    NetworkConnection.online$.subscribe(e => {
      map(e => e)
      NetworkConnection.status = ConnectionStatusEnum.Online;
    });
    console.log(NetworkConnection.online$)

    NetworkConnection.offline$.subscribe(e => {
      console.log('Offline');
      NetworkConnection.status = ConnectionStatusEnum.Offline;
    });
  }

  constructor() {
    NetworkConnection.init();
  }
}

new NetworkConnection();