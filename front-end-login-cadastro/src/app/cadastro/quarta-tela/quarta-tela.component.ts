import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControlName, FormGroup, PatternValidator, Validators } from '@angular/forms';


@Component({
  selector: 'app-quarta-tela',
  templateUrl: './quarta-tela.component.html',
  styleUrls: ['./quarta-tela.component.css']
})
export class QuartaTelaComponent implements OnInit {
 
  formGoupSenha: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    
    this.formGoupSenha = this.formBuilder.group({
      senha: ['', [Validators.required,
        Validators.pattern("^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$")  
      ]],
      confirmarSenha: ['', Validators.required,
        Validators.pattern("^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$")
      ]
    });
  }

  ngOnInit() {

  }

  //Função que verifica o tamanho da senha digítada 
  verificarSenha() {
    if (this.formGoupSenha.value.senha.length >= 8 && this.formGoupSenha.value.confirmarSenha.length >= 8){
      this.compararSenha()
    }else { 
      console.log(this.formGoupSenha.value.senha)
      alert('Senha não possui a quantidade mínima de caracteres')
    }
  }
  
  /*Função que verifica a complexidade da senha Falta ajustar essa função*/
  /*complexidadeSenha() {
    console.log(this.formGoupSenha.value.senha)
    if (this.formGoupSenha.value.senha.PatternValidator) {
      this.compararSenha()
    } else {
      alert('A senha deve conter no mínimo 1 letra Maiúscula, 1 número, e um caracter especial')
    }
  }*/

  //Função que verifica se as senhas são iguais 
  compararSenha() {
    if (this.formGoupSenha.value.senha === this.formGoupSenha.value.confirmarSenha) {
      alert('Senha cadastrada com sucesso')
    } else {
      alert('As senhas não coincidem')
    }
  }
}

