import { Component, ContentChild, OnInit } from '@angular/core';
import { SharedService } from '../shared/shared.service';
import { AuthenticationService } from '../authentication/services/authentication.service';
import { Router } from '@angular/router';
import { Client } from '../shared/interfaces/client';
import {
  FormBuilder,
  FormControlName,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  input: any;

  usuario?: Client;

  @ContentChild(FormControlName) control!: FormControlName;

  constructor(
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private authService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.input = this.control;

    this.loginForm = this.formBuilder.group({
      cpf: this.formBuilder.control('', [
        Validators.required,
        Validators.pattern(this.sharedService.regexValidarCpf),
      ]),
      senha: this.formBuilder.control('', [
        Validators.required,
        Validators.pattern(this.sharedService.regexValidarSenha),
      ]),
    });
  }

  getUserByCpf(cpf: string): void {
    this.authService.getAllUsers().subscribe((users) => {
      this.usuario = users.filter((user) => user.cpf === cpf)[0];
      this.autenticarUsuario(this.usuario)
        ? this.redirecionar()
        : this.naoAutenticou();
    });
  }

  login(): void {
    if (!this.loginForm.valid) {
      return;
    }

    this.getUserByCpf(this.loginForm.controls['cpf'].value);
  }

  autenticarUsuario(usuario: Client): boolean {
    if (!usuario) {
      return false;
    }

    if (!(this.loginForm.controls['senha'].value === usuario.senha)) {
      return false;
    }

    return true;
  }

  redirecionar(): void {
    this.authService.usuarioLogado = true;
    this.router.navigate(['/inicio']);
  }

  naoAutenticou(): void {
    this.authService.usuarioLogado = false;
    alert('CPF ou senha incorretos');
  }
}
