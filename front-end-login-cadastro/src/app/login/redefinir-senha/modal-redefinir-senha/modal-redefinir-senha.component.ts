import { Component, OnInit, Input, ContentChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {
  FormBuilder,
  FormControlName,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-modal-redefinir-senha',
  templateUrl: './modal-redefinir-senha.component.html',
  styleUrls: ['./modal-redefinir-senha.component.css'],
})
export class ModalRedefinirSenhaComponent implements OnInit {
  @Input() name: String | undefined;

  emailPattern = /^[a-z0-9.]+@[a-z0-9]+\.[a-z]+\.([a-z]+)?$/i;

  telefonePattern = /(\(?\d{2}\)?\s)?(\d{4,5}\-\d{4})/;

  redefinirSenhaForm!: FormGroup;
  input: any;

  @ContentChild(FormControlName) control!: FormControlName;

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.input = this.control;

    this.redefinirSenhaForm = this.formBuilder.group({
      email: this.formBuilder.control('', [
        Validators.pattern(this.emailPattern),
      ]),
      telefone: this.formBuilder.control('', [
        Validators.pattern(this.telefonePattern),
      ]),
    });
  }
}
