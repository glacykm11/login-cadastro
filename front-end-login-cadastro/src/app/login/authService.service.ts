import { Client } from '../shared/interfaces/client';
import { CadastroClienteService } from '../cadastro/services/cadastro-cliente.service';
import { EventEmitter, Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  _form!: FormGroup;

  form = new BehaviorSubject<FormGroup>(this._form);

  receiveClient!: Client;

  clientAuthentication: boolean = false;

  constructor(private cadastro: CadastroClienteService) {
    this.form.subscribe((form) => this.auth(form));
  }

  auth(form: FormGroup) {
    if (form) {
      this.cadastro.get().subscribe((cadastro) => {
        this.receiveClient = cadastro.filter(
          (cadastro) => cadastro.cpf == form.get('cpf')?.value
        )[0];
        console.log(this.receiveClient);
        if (
          this.receiveClient &&
          this.receiveClient.senha == form.get('senha')?.value
        ) {
          console.log('Usuário autenticado!');
          return (this.clientAuthentication = true);
        } else {
          console.log('Usuário NÃO autenticado!');
          return;
        }
      });
    }
  }

  nextForm(form: FormGroup) {
    return this.form.next(form);
  }
}
