package br.com.prowaybank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import br.com.prowaybank.logincadastro.controller.RedefinirSenhaController;
import br.com.prowaybank.logincadastro.model.PessoaFisica;
import br.com.prowaybank.logincadastro.model.PessoaJuridica;

class RedefinirSenhaControllerTest {

	@Test
	public void RedefinirSenhaControllerPFRetornaTrue() {
		RedefinirSenhaController redefinirSenha = new RedefinirSenhaController();
		PessoaFisica pf = new PessoaFisica();
		
		pf.setSenha("teste@123");
		pf.setCpf("02775321321");
		
		String codigoConfirmacao = redefinirSenha.codigo;
		redefinirSenha.alterarSenha(pf, "02775321321", "Abc@@123", "Abc@@123", codigoConfirmacao);
		
		assertEquals("Abc@@123", pf.getSenha());
	}
	
	@Test
	public void RedefinirSenhaControllerPFRetornaFalseSenhasDiferentes() {
		RedefinirSenhaController redefinirSenha = new RedefinirSenhaController();
		PessoaFisica pf = new PessoaFisica();
		
		pf.setSenha("teste@123");
		pf.setCpf("02775321321");
		
		String codigoConfirmacao = "123456";

		redefinirSenha.alterarSenha(pf, "02775321321", "123@abc","123@abD", codigoConfirmacao);
		assertNotEquals("123@abc", pf.getSenha());
	}
	
	@Test
	public void RedefinirSenhaControllerPFRetornaFalseCodigoInvalido() {
		RedefinirSenhaController redefinirSenha = new RedefinirSenhaController();
		PessoaFisica pf = new PessoaFisica();
		
		pf.setSenha("teste@123");
		pf.setCpf("02775321321");
		
		String codigoConfirmacao = "123456";

		redefinirSenha.alterarSenha(pf, "02775321321", "123@abc", "123@abc", codigoConfirmacao);
		assertNotEquals("123@abc", pf.getSenha());
	}
	
	@Test
	public void RedefinirSenhaControllerPFRetornaFalseCPFDivergentes() {
		RedefinirSenhaController redefinirSenha = new RedefinirSenhaController();
		PessoaFisica pf = new PessoaFisica();
		
		pf.setSenha("teste@123");
		pf.setCpf("02775321321");
		
		String codigoConfirmacao = redefinirSenha.codigo;
		
		redefinirSenha.alterarSenha(pf, "02775321300", "123@abc", "123@abc",codigoConfirmacao);
		assertNotEquals("123@abc", pf.getSenha());
	}
	
	@Test
	public void RedefinirSenhaControllerPJRetornaTrue() {
		RedefinirSenhaController redefinirSenha = new RedefinirSenhaController();
		PessoaJuridica pj = new PessoaJuridica();
		
		pj.setSenha("Teste@123");
		pj.setCnpj("08722653000196");		
		
		String codigoConfirmacao = redefinirSenha.codigo;
		redefinirSenha.alterarSenha(pj, "08722653000196", "Abc@@123", "Abc@@123", codigoConfirmacao);
		
		assertEquals("Abc@@123", pj.getSenha());
	}
	
	@Test
	public void RedefinirSenhaControllerPJRetornaFalseSenhasDiferentes() {
		RedefinirSenhaController redefinirSenha = new RedefinirSenhaController();
		PessoaJuridica pj = new PessoaJuridica();
		
		pj.setSenha("teste@123");
		pj.setCnpj("08722653000196");
		
		String codigoConfirmacao = "123456";

		redefinirSenha.alterarSenha(pj, "08722653000196", "123@abc","123@abD", codigoConfirmacao);
		assertNotEquals("123@abc", pj.getSenha());
	}
	
	@Test
	public void RedefinirSenhaControllerPJRetornaFalseCNPJDivergentes() {
		RedefinirSenhaController redefinirSenha = new RedefinirSenhaController();
		PessoaJuridica pj = new PessoaJuridica();
		
		pj.setSenha("teste@123");
		pj.setCnpj("08722653000196");		
		
		String codigoConfirmacao = redefinirSenha.codigo;
		
		redefinirSenha.alterarSenha(pj, "08722653000196", "123@abcd", "123@abcd", codigoConfirmacao);
		assertNotEquals("123@abcd", pj.getSenha());
	}
	
	@Test
	public void RedefinirSenhaControllerPJRetornaFalseCodigoInvalido() {
		RedefinirSenhaController redefinirSenha = new RedefinirSenhaController();
		PessoaJuridica pj = new PessoaJuridica();
		
		pj.setSenha("teste@123");
		pj.setCnpj("08722653000196");
		
		String codigoConfirmacao = "123456";

		redefinirSenha.alterarSenha(pj, "08722653000196", "123@abc", "123@abc", codigoConfirmacao);
		assertNotEquals("123@abc", pj.getSenha());
	}

}
