package br.com.prowaybank.controller;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;

import br.com.prowaybank.logincadastro.controller.CriarContaController;
import br.com.prowaybank.logincadastro.model.PessoaFisica;
import br.com.prowaybank.logincadastro.model.PessoaJuridica;

class CriarContaControllerTest {

	@Test
	public void testDadosValidosRetornaTrue() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date data = sdf.parse("25/08/2003");
		boolean test = CriarContaController.dadosValidos("1187654321", data);
		assertTrue(test);
	} 
	
	@Test
	public void testDadosValidosRetornaFalseComDataInvalida() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date data = sdf.parse("25/02/2005");
		boolean test = CriarContaController.dadosValidos("1187654321", data);
		assertFalse(test);
	}

	@Test
	public void testDadosValidosRetornaFalseComTelefoneInvalido() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date data = sdf.parse("25/02/2005");
		boolean test = CriarContaController.dadosValidos("11876543", data);
		assertFalse(test);
	}
	
	@Test
	public void testDadosValidosRetornaFalseComCPFInvalido() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date data = sdf.parse("25/02/2005");
		boolean test = CriarContaController.dadosValidos("11876543", data);
		assertFalse(test);
		
	}
	
	@Test
	public void novoCadastroPfRetornaTrueComDadosValidosESenhasValidasIguais() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date dataTeste = sdf.parse("25/10/2000");
		PessoaFisica pessoaF = new PessoaFisica();
		pessoaF.setCpf("250.106.720-73");
		pessoaF.setDataNascimento(dataTeste);
		pessoaF.setTelefone("1187654321");
		String senha1 = "Abc123@@";
		String senha2 = "Abc123@@";
		boolean teste = CriarContaController.criarConta(pessoaF, senha1, senha2) != null;
		assertTrue(teste);	
	}
	
	@Test
	public void novoCadastroPfRetornaFalseComDadosValidosESenhasDiferentes() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date dataTeste = sdf.parse("25/10/2000");
		PessoaFisica pessoaF = new PessoaFisica();
		pessoaF.setCpf("250.106.720-73");
		pessoaF.setDataNascimento(dataTeste);
		pessoaF.setTelefone("1187654321");
		String senha1 = "Abc123@@";
		String senha2 = "Abc124@@";
		boolean teste = CriarContaController.criarConta(pessoaF, senha1, senha2) != null;
		assertFalse(teste);	
	}
	
	@Test
	public void novoCadastroPfRetornaFalseComCPFInvalidosESenhasIguais() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date dataTeste = sdf.parse("25/10/2000");
		PessoaFisica pessoaF = new PessoaFisica();
		pessoaF.setCpf("145.156.820-64");
		pessoaF.setDataNascimento(dataTeste);
		pessoaF.setTelefone("1187654321");
		String senha1 = "Abc123@@";
		String senha2 = "Abc123@@";
		boolean teste = CriarContaController.criarConta(pessoaF, senha1, senha2) != null;
		assertFalse(teste);
	}
	
	@Test
	public void novoCadastroPfRetornaFalseComDataInvalidaESenhasIguais() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date dataTeste = sdf.parse("25/10/2005");
		PessoaFisica pessoaF = new PessoaFisica();
		pessoaF.setCpf("250.106.720-73");
		pessoaF.setDataNascimento(dataTeste);
		pessoaF.setTelefone("1187654321");
		String senha1 = "Abc123@@";
		String senha2 = "Abc123@@";
		boolean teste = CriarContaController.criarConta(pessoaF, senha1, senha2) != null;
		assertFalse(teste);	
	}
	
	@Test
	public void novoCadastroPfRetornaFalseComTelefoneInvalidoESenhasIguais() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date dataTeste = sdf.parse("25/10/2000");
		PessoaFisica pessoaF = new PessoaFisica();
		pessoaF.setCpf("250.106.720-73");
		pessoaF.setDataNascimento(dataTeste);
		pessoaF.setTelefone("00999884221");
		String senha1 = "Abc123@@";
		String senha2 = "Abc123@@";
		boolean teste = CriarContaController.criarConta(pessoaF, senha1, senha2) != null;
		assertFalse(teste);	
	}
	
	@Test
	public void novoCadastroPfRetornaFalseComCPFJaCadastradoESenhasIguais() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date dataTeste = sdf.parse("25/10/2000");
		PessoaFisica pessoaF = new PessoaFisica();
		pessoaF.setCpf("394.327.210-94");
		pessoaF.setDataNascimento(dataTeste);
		pessoaF.setTelefone("1187654321");
		String senha1 = "Abc123@@";
		String senha2 = "Abc123@@";
		boolean teste = CriarContaController.criarConta(pessoaF, senha1, senha2) != null;
		assertFalse(teste);	
	}
	
	@Test
	public void novoCadastroPjRetornaTrueComCNPJValidoESenhasIguais() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date dataTeste = sdf.parse("25/10/2000");
		PessoaJuridica pessoaJ = new PessoaJuridica();
		pessoaJ.setCnpj("57056858000194");
		pessoaJ.setDataEmpresa(dataTeste);
		pessoaJ.setTelefone("1187654321");
		String senha1 = "Abc123@@";
		String senha2 = "Abc123@@";
		boolean teste = CriarContaController.criarConta(pessoaJ, senha1, senha2) != null;
		assertTrue(teste);	
	}
	
	@Test
	public void novoCadastroPjRetornaFalseComCNPJExistenteESenhasIguais() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date dataTeste = sdf.parse("25/10/2000");
		PessoaJuridica pessoaJ = new PessoaJuridica();
		pessoaJ.setCnpj("57056858000195");
		pessoaJ.setDataEmpresa(dataTeste);
		pessoaJ.setTelefone("1187654321");
		String senha1 = "Abc123@@";
		String senha2 = "Abc123@@";
		boolean teste = CriarContaController.criarConta(pessoaJ, senha1, senha2) != null;
		assertFalse(teste);	
	}

}
