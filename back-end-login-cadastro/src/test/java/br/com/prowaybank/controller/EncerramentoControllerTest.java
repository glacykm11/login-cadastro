package br.com.prowaybank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import br.com.prowaybank.logincadastro.controller.EncerramentoController;
import br.com.prowaybank.logincadastro.model.ContaBancaria;
import br.com.prowaybank.logincadastro.model.PessoaFisica;

class EncerramentoControllerTest {

	@Test
	public void EncerramentoControllerPFRetornaFalse() {
		EncerramentoController controller = new EncerramentoController();
		
		PessoaFisica usuario = new PessoaFisica();
		ContaBancaria conta = new ContaBancaria();
		BigDecimal saldo = new BigDecimal(10.000);
		
		conta.setValorEmConta(saldo);
		usuario.setContaBancaria(conta);
		usuario.setCpf("02775321321");
		
		String retorno = controller.encerrarContaCliente(usuario, usuario.getCpf());
		assertEquals(retorno, "Usuário possui saldo em conta corrente. Impossível encerrar a conta.");
	}

	@Test
	public void EncerramentoControllerPFDesativadaRetornaFalse() {
		EncerramentoController controller = new EncerramentoController();
		
		PessoaFisica usuario = new PessoaFisica();
		ContaBancaria conta = new ContaBancaria();
		BigDecimal saldo = new BigDecimal(15.000);
		
		conta.setAtiva(false);
		conta.setValorEmConta(saldo);
		usuario.setContaBancaria(conta);
		usuario.setCpf("02775321321");
		
		String retorno = controller.encerrarContaCliente(usuario, usuario.getCpf());
		assertEquals(retorno, "Usuário não possui conta corrente ativa. Impossível encerrar a conta.");
	}

	@Test
	public void EncerramentoControllerPFRetornaTrue() {
		EncerramentoController controller = new EncerramentoController();
		
		PessoaFisica usuario = new PessoaFisica();
		ContaBancaria conta = new ContaBancaria();
		BigDecimal saldo = new BigDecimal(0.0);

		conta.setValorEmConta(saldo);
		usuario.setContaBancaria(conta);
		usuario.setCpf("02775321321");
		
		String retorno = controller.encerrarContaCliente(usuario, usuario.getCpf());
		assertEquals(retorno, "Conta encerrada com sucesso");
	} 
}
