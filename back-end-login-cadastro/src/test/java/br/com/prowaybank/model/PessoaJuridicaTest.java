package br.com.prowaybank.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;

import org.junit.jupiter.api.Test;

import br.com.prowaybank.logincadastro.model.PessoaJuridica;
import br.com.prowaybank.logincadastro.util.ValidatorUtils;

class PessoaJuridicaTest {

	@Test
    public void telefoneNoPadraoCorretoEmpresa() {
        PessoaJuridica telefone = new PessoaJuridica();
        telefone.setTelefone("11987654321");
        String telefoneEmpresa = telefone.getTelefone();
        assertEquals("11987654321", telefoneEmpresa);
    }
  @Test
    public void telefoneNoPadraoErradompresa() {
        PessoaJuridica telefone = new PessoaJuridica();
        telefone.setTelefone("11-987654321");
        String telefoneEmpresa = telefone.getTelefone();
       assertNull(telefoneEmpresa); // forma para gerar o erro forçado
    }
  
  @Test 
    public void cpfRepresentantePadraoCorreto() {
        PessoaJuridica representante = new PessoaJuridica();
        representante.setCpfRepresentante("92569048020"); 
        String cpf= representante.getCpfRepresentante();
       assertEquals("92569048020",cpf); 
    }
  
  @Test
    public void cpfRepresentantePadraoErrado() {
        PessoaJuridica representante = new PessoaJuridica();
        representante.setCpfRepresentante("92569048021"); 
        String cpf= representante.getCpfRepresentante();
       assertNull(cpf);
    }
  
  @Test
  public void verificarCnpj( ) {
	  PessoaJuridica verificar = new PessoaJuridica();
	  verificar.setCnpj("23777888000155");
	  String cnpj= verificar.getCnpj();
	  assertEquals("23777888000155",cnpj);
  }
  @Test
  public void verificarErroComNumeracaoMaiorCnpj( ) {
	  PessoaJuridica verificar = new PessoaJuridica();
	  verificar.setCnpj("237778880001569"); 
	  String cnpj= verificar.getCnpj();
	  assertNull(cnpj);
  }
  
	@Test
	public void validarDataDeCriacaoCorreto() throws ParseException {
		String data = "28/09/2020";
		boolean test = ValidatorUtils.dataCriacao( data);
		assertTrue(test);
	} 
  
	@Test
	public void validarDataDeCriacaoErrado() throws ParseException {
		String data = "28/09/2022";
		boolean test = ValidatorUtils.dataCriacao( data);
		assertFalse(test);
	} 

}
