package br.com.prowaybank.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import br.com.prowaybank.logincadastro.model.Endereco;


class EnderecoTest {

	  @Test
	    public void deveGravarCepSomenteSeForValido() {
	        Endereco endereco = new Endereco();
	        endereco.setCep("98765432");
	        String cepCadastrado = endereco.getCep();
	        assertEquals("98765432", cepCadastrado);
	    }

	    @Test
	    public void naoDeveGravarCepSeForInvalido() {
	        Endereco endereco = new Endereco();
	        endereco.setCep("98765");
	        String cepCadastrado = endereco.getCep();
	        assertNull(cepCadastrado);
	    }

}
