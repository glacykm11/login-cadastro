package br.com.prowaybank.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

import br.com.prowaybank.logincadastro.model.PessoaFisica;

class PessoaFisicaTest {

	   @Test
	    public void deveGravarCpfSeEstiverNoPadraoCorreto() {
	        PessoaFisica pf = new PessoaFisica();
	        pf.setCpf("12345678900");
	        String cpfCadastrado = pf.getCpf();
	        assertEquals("12345678900", cpfCadastrado);
	    }

	    @Test
	    public void naoDeveGravarCpfSeEstiverForaDoPadraoCorreto() {
	        PessoaFisica pf = new PessoaFisica();
	        pf.setCpf("123456789");
	        String cpfCadastrado = pf.getCpf();
	        assertNull(cpfCadastrado);
	    }
}
