package br.com.prowaybank.logincadastro.controller;

import java.util.ArrayList;

import br.com.prowaybank.logincadastro.interfaces.EncerraConta;

public class EncerramentoController {
	/**
	 * Encerra conta do usuário
	 * 
	 * @param usuario, documento (CPF ou CNPJ)
	 * @return mensagem de sucesso
	 */
	public String encerrarContaCliente (EncerraConta usuario, String documento) {
		ArrayList<String> pendencias = usuario.getPendencias(documento);
		
		if(pendencias.isEmpty()) {
			usuario.encerrarConta(documento);
			return "Conta encerrada com sucesso";
		} else return pendencias.get(0);
	}
}
