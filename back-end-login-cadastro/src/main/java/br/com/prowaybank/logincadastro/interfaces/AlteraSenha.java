package br.com.prowaybank.logincadastro.interfaces;

public interface AlteraSenha extends ValidaDocumentos{
	
	public void setSenha(String senha);
	public String getSenha();
}
