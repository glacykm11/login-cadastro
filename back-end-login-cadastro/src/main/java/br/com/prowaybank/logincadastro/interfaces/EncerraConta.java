package br.com.prowaybank.logincadastro.interfaces;

public interface EncerraConta extends Pendencias{
	public Boolean encerrarConta(String documento);
}
