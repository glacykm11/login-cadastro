package br.com.prowaybank.logincadastro.interfaces;

import br.com.prowaybank.logincadastro.model.Endereco;

public interface Contato {
	public String getTelefone();
	public String getEmail();
	public Endereco getEndereco();
}
