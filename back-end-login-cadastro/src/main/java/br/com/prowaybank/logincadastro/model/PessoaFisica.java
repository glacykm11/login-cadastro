package br.com.prowaybank.logincadastro.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Entity;

import br.com.prowaybank.logincadastro.interfaces.AlteraSenha;
import br.com.prowaybank.logincadastro.interfaces.CadastroUsuario;
import br.com.prowaybank.logincadastro.interfaces.EncerraConta;
import br.com.prowaybank.logincadastro.util.ValidatorUtils;

@Entity
public class PessoaFisica extends Usuario implements AlteraSenha, EncerraConta, CadastroUsuario {
	
	private String cpf;

	private BigDecimal rendaMensal;

	private Date dataNascimento;

	private Date dataAberturaConta;

	public PessoaFisica() {
	}
	
	public PessoaFisica(String nome, String telefone, String email, Endereco endereco, String cpf,
			BigDecimal rendaMensal, Date dataNascimento) {
		super(nome, telefone, email, endereco);
		this.cpf = cpf;
		this.rendaMensal = rendaMensal;
		this.dataNascimento = dataNascimento;
	}


	/**
	 * CPF do usuario Pessoa Fisica
	 *
	 * @return String com o CPF
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * Modifica o CPF da pessoa jurÃ­dica
	 *
	 * @param cpf
	 */
	public void setCpf(String cpf) {
		if (ValidatorUtils.verificarFormatoCpf(cpf)) {
			this.cpf = cpf;
		}
	}

	/**
	 * Retorna a renda mensal informada pela Pessoa FÃ­sica
	 *
	 * @return BigDecimal renda mensal
	 */
	public BigDecimal getRendaMensal() {
		return rendaMensal;
	}

	/**
	 * Modifica a renda mensal
	 *
	 * @param rendaMensal
	 */
	public void setRendaMensal(BigDecimal rendaMensal) {
		this.rendaMensal = rendaMensal;
	}

	/**
	 * Retorna a data de nascimento
	 * 
	 * @return dataNascimento
	 */
	public Date getDataNascimento() {
		return dataNascimento;
	}

	/**
	 * Salva a data de nascimento
	 * 
	 * @param dataNascimento
	 */
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	/**
	 * Retorna a data que a conta foi criada criacao da conta
	 * @return dataAberturaConta
	 */
	public Date getDataAberturaConta() {
		return dataAberturaConta;
	}

	/**
	 * Salva a data de criacao da conta
	 * @param dataAberturaConta
	 */
	public void setDataAberturaConta(Date dataAberturaConta) {
		this.dataAberturaConta = dataAberturaConta;
	}

	@Override
	public Boolean validarDocumento(String CPF) {
		return ValidatorUtils.verificarFormatoCpf(CPF);
	}

	@Override
	public Boolean verificaDocumentoCadastrado(String CPF) {
		String cpfCadastrado = this.getCpf();
		return cpfCadastrado.equals(CPF);
	}

	@Override
	public Boolean validaCodigoConfirmacao(String codigoInserido, String codigoConfirmacao) {
		return codigoConfirmacao.equals(codigoInserido);
	}

	@Override

	public Date getDataInicial() {
		return this.dataNascimento;
	}

	@Override
	public String getDocumento() {
		return this.cpf;
	}

	@Override
	public BigDecimal getRendimento() {
		return this.rendaMensal;
	}
	
	public ArrayList<String> getPendencias(String documento) {
		ArrayList<String> pendencias = new ArrayList<String>();

		String cpfCadastrado = this.getCpf();
		
		if (cpfCadastrado.equals(documento)){
			if(((Usuario) this).getContaBancaria().getAtiva()) {
				if(((Usuario) this).getContaBancaria().getValorEmConta().compareTo(BigDecimal.ZERO) > 0) {
					pendencias.add("Usuário possui saldo em conta corrente. Impossível encerrar a conta.");
				}
			} else {
				pendencias.add("Usuário não possui conta corrente ativa. Impossível encerrar a conta.");
			}
		}
		return pendencias;
	}

	@Override
	public Boolean encerrarConta(String documento) {
		String cpfCadastrado = this.getCpf();
		
		if (cpfCadastrado.equals(documento)){
			((Usuario) this).getContaBancaria().setAtiva(false);
		}
		
		return ((Usuario) this).getContaBancaria().getAtiva();
	}

}
