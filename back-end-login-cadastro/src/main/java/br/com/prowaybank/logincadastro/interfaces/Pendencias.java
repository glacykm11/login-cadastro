package br.com.prowaybank.logincadastro.interfaces;

import java.util.ArrayList;

public interface Pendencias {
	public ArrayList<String> getPendencias(String documento);
}
