package br.com.prowaybank.logincadastro.interfaces;

public interface ValidaDocumentos {

	public Boolean validarDocumento (String documento);
	
	public Boolean verificaDocumentoCadastrado (String documento);
	
	public Boolean validaCodigoConfirmacao (String codigoInserido, String codigo);
}
