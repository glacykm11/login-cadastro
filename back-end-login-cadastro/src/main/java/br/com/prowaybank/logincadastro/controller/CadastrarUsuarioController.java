package br.com.prowaybank.logincadastro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.prowaybank.logincadastro.interfaces.CadastroUsuario;
import br.com.prowaybank.logincadastro.model.PessoaFisica;
import br.com.prowaybank.logincadastro.model.PessoaJuridica;
import br.com.prowaybank.logincadastro.repository.PessoaFisicaRepository;

@RestController
public class CadastrarUsuarioController {

	@Autowired
	PessoaFisicaRepository pfRepository;
	/**
	 * Cadastra um usuario pessoa fisica
	 * 
	 * O metodo recebe um objeto que e instanciado em um objeto do tipo pessoa fisica e chama o 
	 * metodo criar conta para criar uma conta PF.
	 * @param novaPf
	 * @param senha1
	 * @param senha2
	 * @return 
	 * @throws Exception
	 */
	@PostMapping("/cadastro-pf/{senha1}/{senha2}")
	public void criarPf(@RequestBody PessoaFisica novaPf,@PathVariable("senha1") String senha1,@PathVariable("senha2") String senha2) throws Exception {		
		PessoaFisica usuarioCadastrado = (PessoaFisica) CriarContaController.criarConta(novaPf, senha1, senha2);
		pfRepository.save(usuarioCadastrado);		
	}

	/**
	 * Cadastra um usuario pessoa juridica
	 * 
	 * O metodo recebe um objeto que e instanciado em um objeto do tipo pessoa Juridica e chama o 
	 * metodo criar conta para criar uma conta PJ.
	 * @param novoPj
	 * @param senha1
	 * @param senha2
	 * @return 
	 * @throws Exception
	 */
	@PostMapping("/cadastro-pj/{senha1}/{senha2}")
	public CadastroUsuario criarPj(@RequestBody PessoaJuridica novoPj,@PathVariable String senha1,@PathVariable String senha2) throws Exception {
		
		CadastroUsuario usuarioCadastrado = CriarContaController.criarConta(novoPj, senha1, senha2);
		return usuarioCadastrado;
	}
}
