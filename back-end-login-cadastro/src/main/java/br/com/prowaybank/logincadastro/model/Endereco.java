package br.com.prowaybank.logincadastro.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.com.prowaybank.logincadastro.util.ValidatorUtils;

@Entity
public class Endereco {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String logradouro;

    private String numero;

    private String cep;

    private String cidade;

    private String estado;

    public Endereco() {
    }
    
    /**
     * Id do endereÃ§o
     *
     * @return o Id do endereÃ§o
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Retorna o logradouro (nome da rua, avenida, etc) do endereÃ§o
     *
     * @return logradouro
     */
    public String getLogradouro() {
        return this.logradouro;
    }

    /**
     * Modifica o logradouro
     *
     * @param logradouro
     */
    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    /**
     * Retorna o nÃºmero da rua/avenida/etc do endereÃ§o do usuÃ¡rio
     *
     * @return nÃºmero da rua no formato de string
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Modifica o nÃºmero da rua/avenida/etc
     *
     * @param numero
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * Retorna o CEP do endereÃ§o do usuÃ¡rio
     *
     * @return String CEP
     */
    public String getCep() {
        return cep;
    }

    /**
     * Modifica o CEP do usuÃ¡rio. SÃ³ faz a modificaÃ§Ã£o se o CEP informado estiver dentro do padrÃ£o definido
     * no mÃ©todo validarCep(), que deve receber um CEP ou no formato XXXXXXXX ou XXXXX-XXX
     * @param cep
     */
    public void setCep(String cep) {
        if(ValidatorUtils.validarCep(cep)) {
            this.cep = cep;
        }
    }

    /**
     * Retorna a cidade do usuÃ¡rio
     *
     * @return String cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * Modifica a cidade do usuÃ¡rio
     *
     * @param cidade
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * Retorna o estado (UF) do usuÃ¡rio
     *
     * @return String estado (UF)
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Modifica o estado (UF) do usuÃ¡rio
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
}
