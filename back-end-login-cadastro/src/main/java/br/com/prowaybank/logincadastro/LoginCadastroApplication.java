package br.com.prowaybank.logincadastro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoginCadastroApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoginCadastroApplication.class, args);
	}

}
