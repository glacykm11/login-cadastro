package br.com.prowaybank.logincadastro.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ContaBancaria {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

    private String agencia;

    private String numeroConta;

    private BigDecimal valorEmConta;
    
    private Boolean ativa;

    public ContaBancaria() {
    	this.setAtiva(true);
    }
    
    public ContaBancaria(String agencia, String numeroConta, BigDecimal valorEmConta) {
		this.agencia = agencia;
		this.numeroConta = numeroConta;
		this.valorEmConta = valorEmConta;
		this.setAtiva(true);
	}
    
	/**
     * Retorna o ID do objeto ContaBancaria
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * Retorna a agÃªncia da ContaBancaria
     *
     * @return String agencia
     */
    
    
    
    public String getAgencia() {
        return agencia;
    }

    /**
     * Modifica a agÃªncia vinculada Ã  conta bancÃ¡ria
     *
     * @param agencia
     */
    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    /**
     * Retorna o nÃºmero da conta bancÃ¡ria que estÃ¡ viculada ao cliente
     *
     * @return String nÃºmero da conta
     */
    public String getNumeroConta() {
        return numeroConta;
    }

    /**
     * Modifica o nÃºmero da conta bancÃ¡ria vinculada ao cliente
     *
     * @param numeroConta
     */
    public void setNumeroConta(String numeroConta) {
        this.numeroConta = numeroConta;
    }

    /**
     * Exibe o valor total disponÃ­vel na conta bancÃ¡ria
     *
     * @return BigDecimal com o valor disponÃ­vel
     */
    public BigDecimal getValorEmConta() {
        return valorEmConta;
    }

    /**
     * Modifica o valor disponÃ­vel em conta
     *
     * @param valorEmConta
     */
    public void setValorEmConta(BigDecimal valorEmConta) {
        this.valorEmConta = valorEmConta;
    }

	public Boolean getAtiva() {
		return ativa;
	}

	public void setAtiva(Boolean ativa) {
		this.ativa = ativa;
	}
}
