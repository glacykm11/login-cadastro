package br.com.prowaybank.logincadastro.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import br.com.prowaybank.logincadastro.util.ValidatorUtils;

@Entity
public class Usuario {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id; // permanece

	private String nome; // inteface

	private String telefone; // interface contato

	private String senha;// permanece

	private String email;// intefce contato

	@OneToOne(cascade = CascadeType.ALL)
	private Endereco endereco; // interface contato

	@OneToOne(cascade = CascadeType.ALL)
	private ContaBancaria contaBancaria;

	public Usuario() {
	}

	public Usuario(String nome, String telefone, String email, Endereco endereco) {
		this.nome = nome;
		this.telefone = telefone;
		this.email = email;
		this.endereco = endereco;
	}

	/**
	 * ID do usuÃ¡rio
	 *
	 * @return Long com o id do usuÃ¡rio
	 */
	public Long getId() {

		return id;
	}

	/**
	 * Nome do usuÃ¡rio
	 *
	 * @return string com o nome do usuÃ¡rio
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Modifica o nome do usuÃ¡rio
	 *
	 * @param nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Exibe o telefone do usuÃ¡rio
	 *
	 * @return string contendo o telefone do usuÃ¡rio
	 */
	public String getTelefone() {
		return telefone;
	}

	/**
	 * Modifica o telefone do usuÃ¡rio
	 *
	 * @param telefone
	 */
	public void setTelefone(String telefone) {
		if (ValidatorUtils.validarNumeroTelefone(telefone)) { // aqui
			this.telefone = telefone;
		}
	}

	/**
	 * Exibe o endereÃ§o do usuÃ¡rio
	 *
	 * @return um objeto do tipo Endereco, onde estÃ£o cadastradas todas as
	 *         informaÃ§Ãµes do endereÃ§o
	 */
	public Endereco getEndereco() {
		return endereco;
	}

	/**
	 * Modifica o endereÃ§o do usuÃ¡rio.
	 *
	 * @param endereco Recebe um objeto do tipo Endereco, onde estÃ£o todas as
	 *                 informaÃ§Ãµes do endereÃ§o do usuÃ¡rio
	 */
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	/**
	 * Exibe a conta bancÃ¡ria do cliente. Retorna um objeto do tipo ContaBancaria
	 * que contÃ©m as informaÃ§Ãµes da conta, como agÃªncia e nÃºmero da conta
	 *
	 * @return objeto ContaBancaria com as informaÃ§Ãµes da conta do usuÃ¡rio
	 */
	public ContaBancaria getContaBancaria() {
		return contaBancaria;
	}

	/**
	 * Modifica a conta bancÃ¡ria do usuÃ¡rio
	 *
	 * @param contaBancaria - objeto do tipo ContaBancaria, que contÃ©m a agÃªncia e
	 *                      o nÃºmero da conta
	 */
	public void setContaBancaria(ContaBancaria contaBancaria) {
		this.contaBancaria = contaBancaria;
	}

	/**
	 * Retorna a senha do usuÃ¡rio
	 *
	 * @return String com a senha
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * Modifica a senha do usuÃ¡rio
	 *
	 * @param senha
	 */
	public void setSenha(String senha) {
		if (ValidatorUtils.verificarSenhaDentroDoPadrao(senha)) {
			this.senha = senha;
		}
	}

	/**
	 * Exibe o e-mail do usuÃ¡rio
	 *
	 * @return String contendo o e-mail
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Cadastra o e-mail do usuÃ¡rio
	 *
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

}
